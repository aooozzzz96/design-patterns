
void main() {
  
  print("##### abstract factory #####");
  
  SolitareRingFactory _factory = SolitareRingFactory();
  SolitareRing ring = SolitareRing(_factory);
  
  ring.prepareRing();
  ring.getDescription();
  ring.jewel.getDescription();
  
  print("##### command pattern #####");
  
  Invoker invoker = Invoker();
  
  invoker.setCommand(LightOnCommand());
  invoker.execute();
  
  invoker.setCommand(LightOffCommand());
  invoker.execute();
  
  print("##### singleton #####");
  
  Singleton _instance = Singleton.getInstance();
  print(_instance.hashCode);
  Singleton _instance_1 = Singleton.getInstance();
  print(_instance_1.hashCode);
}

abstract class Ring{
  Jewel jewel;
  RingFactory ringFactory;
  
  void getDescription();
  
  void prepareRing() {
    print("During preparing. ###--#####");
    jewel = ringFactory.addJewel();
  }
}

abstract class RingFactory {
  Jewel addJewel();
}

abstract class Jewel {
  void getDescription();
}

class SolitareJewel extends Jewel {
  void getDescription() {
    print("This is Solitare jewel.");
  }
}

class SolitareRingFactory extends RingFactory {
  
  Jewel addJewel() {
    return SolitareJewel();
  }
  
}

class SolitareRing extends Ring {
  SolitareRing(RingFactory ringFactory) {
    this.ringFactory = ringFactory;
  }
  
  void getDescription() {
    print("This is Solitare ring fabricated by Lazordi.");
  }
}

class Invoker {
  Command _cmd;
  void setCommand(Command cmd){
    _cmd = cmd;
  }
  
  void execute() {
    _cmd.execute();
  }
}

abstract class Command {
  void execute();
}

class LightOnCommand extends Command {
  void execute() {
    print("Light On");
  }
}

class LightOffCommand extends Command {
  void execute() {
    print("Light Off");
  }
}

class Singleton {
  static Singleton _instance;
  
  static Singleton getInstance() {
    if(_instance == null) {
      _instance = Singleton._();
    }
    return _instance;
  }
  
  Singleton._();
  
}
