void main() {
  // Strategy pattern 
  ConcreteStrategy _concreteStrategy = ConcreteStrategy();
  _concreteStrategy.setStrategyBehavior = Beh();
  _concreteStrategy.execute();
  _concreteStrategy.setStrategyBehavior = Beh1();
  _concreteStrategy.execute();
  
  // Decorator pattern 
  Beverage b1 = Espresso();
  b1 = Mocha(b1);
  print(b1.cost());
  Beverage d1 = DarkCoffee();
  d1 = Mocha(d1);
  d1 = Milk(d1);
  print(d1.cost());
  
  // Observer pattern
  ConcreteSubject _concreteSubject = ConcreteSubject();
  Observer _concreteObserver1 = ConcreteObserver1(_concreteSubject);
  Observer _concreteObserver2 = ConcreteObserver2(_concreteSubject);
  _concreteSubject.notifyAll();
}

// Strategy pattern 

abstract class StrategyConcrete{
  StrategyBehavior strategyBehavior;
  void execute();
}

abstract class StrategyBehavior{
  void exceuteInBehavior();
}

class Beh extends StrategyBehavior{
  void exceuteInBehavior() {
    print("Beh behavior");
  }
}

class Beh1 extends StrategyBehavior{
  void exceuteInBehavior() {
    print("Beh1 behavior");
  }
}

class ConcreteStrategy extends StrategyConcrete{
  set setStrategyBehavior(StrategyBehavior v){
    strategyBehavior = v;
  }
  
  void execute() {
    strategyBehavior.exceuteInBehavior();
  }
}

// Decorator pattern

abstract class Beverage {
  void getDescription();
  double cost();
}


class Espresso extends Beverage {
  void getDescription() {
    print("Espresso Cup");
  }
  
  double cost() {
    return 20.0; 
  }
}

class DarkCoffee extends Beverage {
  void getDescription() {
    print("DarkCoffee Cup");
  }
  
  double cost() {
    return 40.0; 
  }
}

abstract class CondminatDecorator extends Beverage{
  Beverage beverage;
  
  void getDescription();
  double cost();
}

class Mocha extends CondminatDecorator{
  Mocha(Beverage bev) {
    beverage = bev;
  }
  
  void getDescription() {
    print("mocha added");
  }
  
  double cost() {
    return 10.0 + beverage.cost();
  }
}

class Milk extends CondminatDecorator{
  Milk(Beverage bev) {
    beverage = bev;
  }
  
  void getDescription() {
    print("milk added");
  }
  
  double cost() {
    return 15.0 + beverage.cost();
  }
}

class SubjectData{
  int i = 0;
}

abstract class Subject{
  void attach(Observer observer);
  void detach(Observer observer);
  void notifyAll();
}

abstract class Observer{
  void update(SubjectData subjectData);
}

class ConcreteSubject extends Subject{
  SubjectData _subjectData = SubjectData();
  List<Observer> _observers;
  
  ConcreteSubject() {
    _observers = <Observer>[];
    _subjectData.i = 10;
  }
  
  void attach(Observer observer) {
    _observers.add(observer);
  }
  
  void detach(Observer observer) {
    if(_observers.contains(observer)) {
      _observers.remove(observer);
    }
  }
  
  void notifyAll() {
    _observers.forEach((observer) {
      observer.update(_subjectData);
    });
  }
}

class ConcreteObserver1 extends Observer{
  ConcreteSubject _concreteSubject;
  
  ConcreteObserver1(ConcreteSubject con) {
    _concreteSubject = con;
    _concreteSubject.attach(this);
    print("Attached: ConcreteObserver1");
  }
  
  void update(SubjectData subjectData){
    print("Update with ConcreteObserver1" + subjectData.i.toString());
  }
}

class ConcreteObserver2 extends Observer{
  ConcreteSubject _concreteSubject;
  
  ConcreteObserver2(ConcreteSubject con) {
    _concreteSubject = con;
    _concreteSubject.attach(this);
    print("Attached: ConcreteObserver2");
  }
  
  void update(SubjectData subjectData){
    print("Update with ConcreteObserver2" + subjectData.i.toString());
  }
}
